#!/bin/bash

PROD_SITE=DEFAULT
if [ -z $PROD_SITE ];
then
    export LIST=PROD_KUBECONFIG;
else
    for item in $(echo $PROD_SITE | sed "s/,/ /g")
    do
        if [[ "default" =~ ${item} ]];
        then
            export LIST=${LIST}PROD_KUBECONFIG,
        else
            export LIST=${LIST}PROD_KUBECONFIG_${item},
        fi;
    done;
    export LIST=${LIST::-1}
fi;

echo $LIST